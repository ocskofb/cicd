import render from './render'

describe("Test render function", () => {
  test("# should be h1 tag", () => {
    expect(render("# Hello!")).toBe("<h1>Hello!</h1>\n");
  });
});
